﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;
using Newtonsoft.Json;

namespace DataComposer
{
    public class DataController
    {
        public DataController()
        {
            this.Path = string.Empty;
            this.Html = string.Empty;
            this.Exercise = new Exercise();
            this.Info = new Info();
        }

        public string Path { get; set; }

        public string Html { get; set; }

        public Exercise Exercise { get; set; }

        public Info Info { get; set; }

        public void Save()
        {
            File.WriteAllText(Path + ".html", Html);
            File.WriteAllText(Path + ".json", JsonConvert.SerializeObject(Exercise));
            File.WriteAllText(Path + ".ini", JsonConvert.SerializeObject(Info));
        }

        public static DataController Load(string path)
        {
            var data = new DataController();

            data.Path = path;

            data.Exercise = JsonConvert.DeserializeObject<Exercise>(File.ReadAllText(path + ".json"));
            data.Html = File.ReadAllText(path + ".html");
            data.Info = JsonConvert.DeserializeObject<Info>(File.ReadAllText(path + ".ini"));

            return data;
        }
    }
}
