﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;

namespace DataComposer
{
    [DataContract]
    public class Question
    {
        [DataMember(Name = "title")]
        public string Title { get; set; }

        [DataMember(Name = "answers")]
        public List<string> Answers { get; set; }

        [DataMember(Name = "correct")]
        public int Correct { get; set; }

        public Question()
        {
            Answers = new List<string>();
        }
    }
}
