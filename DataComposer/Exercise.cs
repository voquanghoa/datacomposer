﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;

namespace DataComposer
{
    [DataContract]
    public class Exercise
    {
        [DataMember(Name = "questions")]
        public List<Question> Questions { get; set; }

        public Exercise()
        {
            Questions = new List<Question>();
        }
    }
}
