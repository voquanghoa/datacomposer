﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;

namespace DataComposer
{
    [DataContract]
    public class Info
    {
        [DataMember(Name = "lao")]
        public string Lao { get; set; }

        [DataMember(Name = "viet")]
        public string Vietnam { get; set; }
    }
}
