﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace DataComposer
{
    public partial class Form1 : Form
    {
        public DataController CurrentData { get; set; } = new DataController();

        public Form1()
        {
            InitializeComponent();
        }

        private void txtHtml_TextChanged(object sender, EventArgs e)
        {
            try
            {
                CurrentData.Html = txtHtml.Text;
                webBrowser1.DocumentText = txtHtml.Text;
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Error", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
            }
        }

        private void btNew_Click(object sender, EventArgs e)
        {
            CurrentData = new DataController();
            DisplayData();
        }

        private void DisplayData()
        {
            txtHtml.Text = CurrentData.Html;
            listQuestion.Items.Clear();
            listQuestion.Items.AddRange(CurrentData.Exercise.Questions.Select(CreateListViewItem).ToArray());
            txtLao.Text = CurrentData.Info.Lao;
            txtViet.Text = CurrentData.Info.Vietnam;
        }
        
        private ListViewItem CreateListViewItem(Question question)
        {
            return new ListViewItem()
            {
                Tag = question,
                Text = string.IsNullOrEmpty(question.Title)?"Empty Question": question.Title,
            };
        }

        private void btSave_Click(object sender, EventArgs e)
        {
            Save(string.IsNullOrEmpty(CurrentData.Path));
        }

        private void Save(bool displayDialog)
        {
            if (displayDialog)
            {
                if(saveFileDialog1.ShowDialog() == DialogResult.Cancel)
                {
                    return;
                }
                else
                {
                    CurrentData.Path = ExtractPath(saveFileDialog1.FileName);
                }
            }
            
            CurrentData.Save();
        }

        public string ExtractPath(string originPath)
        {
            if (string.IsNullOrEmpty(Path.GetExtension(originPath)))
            {
                return originPath;
            }
            return Path.Combine(Path.GetDirectoryName(originPath), Path.GetFileNameWithoutExtension(originPath));
        }

        private void btSaveAs_Click(object sender, EventArgs e)
        {
            Save(true);
        }

        private void listQuestion_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (listQuestion.SelectedItems.Count > 0)
            {
                groupQuestion.Visible = true;

                var txtAnwers = new List<TextBox>() { txtA, txtB, txtC, txtD };
                var txtRadios = new List<RadioButton>() {radio1, radio2, radio3, radio4 };
                var question = (Question)listQuestion.SelectedItems[0].Tag;
                txtTitle.Text = question.Title;
                for (int i = 0; i < txtAnwers.Count; i++)
                {
                    if (question.Answers.Count > i)
                    {
                        txtAnwers[i].Text = question.Answers[i];
                    }
                    else
                    {
                        txtAnwers[i].Text = string.Empty;
                    }
                    if (i == question.Correct)
                    {
                        txtRadios[i].Checked = true;
                    }
                }
            }
            else
            {
                groupQuestion.Visible = false;
            }
        }

        private void button3_Click(object sender, EventArgs e)
        {
            if (listQuestion.SelectedItems.Count > 0)
            {
                var control = listQuestion.SelectedItems[0];
                var question = (Question)control.Tag;
                var txtAnwers = new List<TextBox>() { txtA, txtB, txtC, txtD };

                question.Title = txtTitle.Text;
                control.Text = question.Title;
                while(question.Answers.Count < txtAnwers.Count)
                {
                    question.Answers.Add(string.Empty);
                }

                for (int i = 0; i < txtAnwers.Count; i++)
                {
                    question.Answers[i] = txtAnwers[i].Text;
                }
            }
            else
            {
                ShowMessage("Chưa chọn câu hỏi nào.");
            }
        }

        private void ShowMessage(string message)
        {
            MessageBox.Show(message, "Message", MessageBoxButtons.OK, MessageBoxIcon.Information);
        }

        private void button2_Click(object sender, EventArgs e)
        {
            if (listQuestion.SelectedItems.Count > 0)
            {
                listQuestion.Items.Remove(listQuestion.SelectedItems[0]);
            }
            else
            {
                ShowMessage("Chưa chọn câu hỏi nào.");
            }
        }

        private void button1_Click(object sender, EventArgs e)
        {
            CurrentData.Exercise.Questions.Add(new Question());
            listQuestion.Items.Add(CreateListViewItem(CurrentData.Exercise.Questions.Last()));
        }

        private void btOpen_Click(object sender, EventArgs e)
        {
            try
            {
                if (openFileDialog1.ShowDialog() != DialogResult.Cancel)
                {
                    CurrentData = DataController.Load(ExtractPath(openFileDialog1.FileName));
                    DisplayData();
                }
            }
            catch(Exception ex)
            {
                ShowMessage(ex.Message);
            }
            
        }

        private void txtViet_TextChanged(object sender, EventArgs e)
        {
           CurrentData.Info.Vietnam = txtViet.Text;

        }

        private void txtLao_TextChanged(object sender, EventArgs e)
        {
            CurrentData.Info.Lao = txtLao.Text;
        }

        private void radio4_Click(object sender, EventArgs e)
        {
            if (((RadioButton)sender).Checked)
            {
                var txtRadios = new List<RadioButton>() { radio1, radio2, radio3, radio4 };
                var question = (Question)listQuestion.SelectedItems[0].Tag;
                question.Correct = txtRadios.IndexOf((RadioButton)sender);
            }
        }
    }
}
